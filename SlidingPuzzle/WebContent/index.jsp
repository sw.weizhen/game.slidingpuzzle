<%@ page language="java" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page pageEncoding="UTF-8" %>
    
<!DOCTYPE html>
<html>
	<head>
		<title>8 Puzzle JSP Project</title>
		<link rel="stylesheet" href="./SRC/css/main.css" type="text/css" />
	</head>
	<body>
		<div id = "controlPanel">
			<button class = "button" id = "btnSplit">SPLIT</button><br>
			<button class = "button" id = "btnHint">HINT</button><br>
			<button class = "button" id = "btnAuto">Automate</button><br>
			<input type = "text" id = "javaBeanTransConcurrent" style="display:none;"/><br>
			<input type = "text" id = "javaBeanTransCommand" style="display:none;"/>
		</div>

		<div id = "puzzleSelect">
			<img id = "img00" class = "srcImg" data-mapping="orgImg00" src = "./SRC/puzzleSource/img00.jpg">
			<img id = "img01" class = "srcImg" data-mapping="orgImg01" src = "./SRC/puzzleSource/img01.jpg">
			<img id = "img02" class = "srcImg" data-mapping="orgImg02" src = "./SRC/puzzleSource/img02.jpg">
			<img id = "img03" class = "srcImg" data-mapping="orgImg03" src = "./SRC/puzzleSource/img03.jpg">
			<img id = "img04" class = "srcImg" data-mapping="orgImg04" src = "./SRC/puzzleSource/img04.jpg">
		</div>
		
		<div id = "puzzleContainer">
			<!-- 
			<div id ="animate"></div>
			 -->
			<canvas id = "canvas1" data-orgPos = "1" data-tmpPos = "-1"></canvas>
			<canvas id = "canvas2" data-orgPos = "2" data-tmpPos = "-1"></canvas>
			<canvas id = "canvas3" data-orgPos = "3" data-tmpPos = "-1"></canvas>
			<canvas id = "canvas4" data-orgPos = "4" data-tmpPos = "-1"></canvas>
			<canvas id = "canvas5" data-orgPos = "5" data-tmpPos = "-1"></canvas>
			<canvas id = "canvas6" data-orgPos = "6" data-tmpPos = "-1"></canvas>
			<canvas id = "canvas7" data-orgPos = "7" data-tmpPos = "-1"></canvas>
			<canvas id = "canvas8" data-orgPos = "8" data-tmpPos = "-1"></canvas>
			<canvas id = "canvas9" data-orgPos = "0" data-tmpPos = "-1"></canvas>
		</div>
		
		<canvas id="canvasMain"></canvas>
		
		
		<img id = "orgImg00" class = "orgImg" src = "./SRC/puzzleSource/img00.jpg">
		<img id = "orgImg01" class = "orgImg" src = "./SRC/puzzleSource/img01.jpg">
		<img id = "orgImg02" class = "orgImg" src = "./SRC/puzzleSource/img02.jpg">
		<img id = "orgImg03" class = "orgImg" src = "./SRC/puzzleSource/img03.jpg">
		<img id = "orgImg04" class = "orgImg" src = "./SRC/puzzleSource/img04.jpg">
		
		<div id = "btnGroup" style="display:none;">
			<label id = "lblDebugView">Debug View</label></br>
			<textarea id = "debugView" rows="20" cols="22"></textarea>
		</div>
		
		<label id = "lblHint"></label>

		<script src="./SRC/javascript/main.js"></script>
		<script src="./SRC/jquery/jquery-1.9.1.min.js"></script>
	</body>
</html>