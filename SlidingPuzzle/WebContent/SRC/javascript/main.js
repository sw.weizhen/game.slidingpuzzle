var imgArray = 	["img00", "img01", "img02", "img03", "img04"];

var canvasArray = 	[
						["canvas1", "canvas2", "canvas3"],
						["canvas4", "canvas5", "canvas6"],
						["canvas7", "canvas8", "canvas9"]
					];

var concurrentCanvasArray = [
								[1, 2, 3],
								[4, 5, 6],
								[7, 8, 0]
							];

var tmpCoorArray = [
						[0, 0],
						[0, 0],
						[0, 0],
						[0, 0],
						[0, 0],
						[0, 0],
						[0, 0],
						[0, 0],
						[0, 0],
					];

var moveDirection = [
						[0, 0, 0, 0],
						[2, 4, 0, 0], //1
						[1, 3, 5, 0], //2
						[2, 6, 0, 0], //3
						[1, 5, 7, 0], //4
						[2, 4, 6, 8], //5
						[3, 5, 9, 0], //6
						[4, 8, 0, 0], //7
						[5, 7, 9, 0], //8
						[6, 8, 0, 0]  //9
					];

var moveDirString = [
						["None", 	"None", 	"None", 	"None"],
						["Right", 	"Down", 	"None", 	"None"], //1
						["Left", 	"Right", 	"Down", 	"None"], //2
						["Left", 	"Down", 	"None", 	"None"], //3
						["Up", 		"Right", 	"Down", 	"None"], //4
						["Up", 		"Left", 	"Right", 	"Down"], //5
						["Up", 		"Left", 	"Down", 	"None"], //6
						["Up", 		"Right", 	"None", 	"None"], //7
						["Up", 		"Left", 	"Right", 	"None"], //8
						["Up", 		"Left", 	"None", 	"None"], //9
					];

var command 	= 	["Up", "Down", "Left", "Right"];
var command_s	=	["&uarr;", "&darr;", "&larr;", "&rarr;"];
var steps		= 	[];

var freeSpace = 9;

var orgWidth 	= 0;
var orgHeight 	= 0;

var picGap = 5;

var secW = 0;
var secH = 0;

var startX = 2;
var startY = 2;

var count = 0;

var isProcessing = false;

//=========================================================================================================================================
function splitImg() {
	var imgObj = document.getElementById($(this).data("mapping"));
	
	orgWidth	= imgObj.width;
    orgHeight	= imgObj.height;
    
    secW = orgWidth / 3;
    secH = orgHeight / 3;

    canvasMain.width	= orgWidth;
    canvasMain.height	= orgHeight;
    
    canvasMain.style.width	= orgWidth;
    canvasMain.style.height	= orgHeight;

	var x = 0;
	var y = 0;
	var split;
	
	var pX = 0; 
	var pY = 0;
	var k = 0;
	var dataPos = 1;
	
	for(var i = 0; i < canvasArray.length; i ++) {
		for(var j = 0; j < canvasArray[0].length; j ++) {
			
			document.getElementById(canvasArray[i][j]).width	= secW;
	    	document.getElementById(canvasArray[i][j]).height	= secH;
			
			split = canvasMain.getContext("2d");
			split.rect(x, y, secW, secH);
			split.drawImage(imgObj, 0, 0);
			document.getElementById(canvasArray[i][j]).getContext("2d").drawImage(canvasMain, x, y, secW, secH, 0, 0, secW, secH);

		    document.getElementById(canvasArray[i][j]).style.top	= pY + "px";
			document.getElementById(canvasArray[i][j]).style.left	= pX + "px";
			
			$(document.getElementById(canvasArray[i][j])).data("orgPos", dataPos);
			$(document.getElementById(canvasArray[i][j])).data("tmpPos", dataPos ++);
			
			tmpCoorArray[k][0] = pX;
			tmpCoorArray[k][1] = pY;

			pX += secW + picGap;
			
			x += secW;
			k ++;
		}
		
		x = 0;
		y += secH;
		
		pX = 0;
		pY += secH + picGap;
	}

	freeSpace = -1;
}

//=========================================================================================================================================
function randomDismiss() {
	var randomList = [];
	var randomPos;
	
	for(;;) {
		randomPos = parseInt(Math.floor(Math.random() * 8) + 1);
		if(randomList.indexOf(randomPos) == -1) randomList.push(randomPos);
		if(randomList.length >= 8) break;
	}
	
	var tmpPosition = 0;
	var k = 0;
	for(var i = 0; i < canvasArray.length; i ++) {
		for(var j = 0; j < canvasArray[0].length; j ++) { 
			$(document.getElementById(canvasArray[i][j])).data("tmpPos", randomList[k ++]);
			
			tmpPosition = parseInt($(document.getElementById(canvasArray[i][j])).data("tmpPos"));
			document.getElementById(canvasArray[i][j]).style.top = tmpCoorArray[tmpPosition - 1][1] + "px";
			document.getElementById(canvasArray[i][j]).style.left = tmpCoorArray[tmpPosition - 1][0] + "px";
			
		}
	}
	document.getElementById(canvasArray[canvasArray.length - 1][canvasArray[0].length - 1]).style.left = (secW * (canvasArray.length) + picGap * 3) + "px";
	
	freeSpace = 9;
	getConcurrentCanvasArray();
	lblHint.innerHTML = "New Game";
}

//=========================================================================================================================================
function getConcurrentCanvasArray() {
	var tmpPos = 0;
	var orgPos = 0;
	
	var ei 	= 0;
	var ej 	= 0
	
	for(var i = 0; i < canvasArray.length; i ++) {
		for(var j = 0; j < canvasArray[0].length; j ++) {
			tmpPos 	= $(document.getElementById(canvasArray[i][j])).data("tmpPos");
			orgPos 	= $(document.getElementById(canvasArray[i][j])).data("orgPos");
			ei 		= parseInt((tmpPos - 1) / 3);
			ej 		= (tmpPos - 1) % 3;
			
			orgPos == 9 ? orgPos = 0: orgPos = orgPos;
			concurrentCanvasArray[ei][ej] = orgPos;
		}
	}
	
	var transConcurrent = "";
	for(var i = 0; i < concurrentCanvasArray.length; i ++) 
		for(var j = 0; j < concurrentCanvasArray[0].length; j ++)
			transConcurrent += concurrentCanvasArray[i][j] + ",";
	javaBeanTransConcurrent.value = transConcurrent.substring(0, transConcurrent.length - 1);
}

//=========================================================================================================================================
function isAccomplished() {
	var result = true;
	var orgPos = 0;
	var tmpPos = 0;
	for(var i = 0; i < canvasArray.length; i ++) {
		for(var j = 0; j < canvasArray[0].length; j ++) { 
			orgPos = $(document.getElementById(canvasArray[i][j])).data("orgPos");
			tmpPos = $(document.getElementById(canvasArray[i][j])).data("tmpPos");
			if(parseInt(orgPos) != parseInt(tmpPos)) return false;
		}
	}
	
	return result;
}

//=========================================================================================================================================
function isSolvable() {
	var arrayPuzzle = [];
	var k = 0; 
	for(var i = 0; i < concurrentCanvasArray.length; i ++)
		for(var j = 0; j < concurrentCanvasArray[0].length; j ++)
			arrayPuzzle.push(concurrentCanvasArray[i][j]);
	
	var inversions = 0;
    for(var i = 0; i < arrayPuzzle.length - 1; i ++) {
      for(var j = i + 1; j < arrayPuzzle.length; j ++) 
    	  if(arrayPuzzle[i] > arrayPuzzle[j]) 
    		  inversions ++;  
      
      if(arrayPuzzle[i] == 0 && i % 2 == 1)
    	  inversions ++;
    }

    return (inversions % 2 == 0);
}

//=========================================================================================================================================
function swap(i, j, ei, ej) {
	var tmpValue 					= concurrentCanvasArray[i][j];
	concurrentCanvasArray[i][j] 	= concurrentCanvasArray[ei][ej];
	concurrentCanvasArray[ei][ej] 	= tmpValue;
}

//=========================================================================================================================================
function brickTrans() {
	if(!isAccomplished() && isProcessing == false) {
		var tmpPosition = $(this).data("tmpPos");
		var orgPosition = $(this).data("orgPos");
		var dir = 0;
		var direction = moveDirection[tmpPosition];
		
		var concurrent_i = parseInt((tmpPosition - 1) / 3);
		var concurrent_j = (tmpPosition - 1) % 3;
		
		for(var i = 0; i < direction.length; i ++) {
			dir = direction[i];
			if(dir != 0 && (parseInt(dir) == parseInt(freeSpace)) && orgPosition != 9) {
				var getCoordinate	= tmpCoorArray[freeSpace - 1];
				this.style.left		= getCoordinate[0] + "px";
				this.style.top		= getCoordinate[1] + "px";
				
				$(this).data("tmpPos", freeSpace);
				freeSpace = tmpPosition;
			}
		}
		
		swap(concurrent_i, concurrent_j, parseInt(($(this).data("tmpPos") - 1) / 3), ($(this).data("tmpPos") - 1) % 3);
		startX = concurrent_i;
		startY = concurrent_j;
		
		var transConcurrent = "";
		for(var i = 0; i < concurrentCanvasArray.length; i ++) 
			for(var j = 0; j < concurrentCanvasArray[0].length; j ++)
				transConcurrent += concurrentCanvasArray[i][j] + ",";
		javaBeanTransConcurrent.value = transConcurrent.substring(0, transConcurrent.length - 1);

		if(isAccomplished()) {
			document.getElementById(canvasArray[canvasArray.length - 1][canvasArray[0].length - 1]).style.left = tmpCoorArray[8][0] + "px";
			freeSpace = -1;
			getConcurrentCanvasArray();
			
			lblHint.innerHTML = "Goal";
			
			//location.reload();
		}
	}
}
//=========================================================================================================================================
function getSteps() {
	var req = javaBeanTransConcurrent.value;
	var ajax;
	if(window.XMLHttpRequest) 
		ajax = new XMLHttpRequest();
	else if(window.ActiveXObject) 
		ajax = new ActiveXObject("MSXML2.XMLHTTP.3.0");
	
	if(ajax) {
		ajax.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	javaBeanTransCommand.value = this.responseText.trim();
		    	if(javaBeanTransCommand.value.trim() != "") 
		    		steps = javaBeanTransCommand.value.trim().split(",");
		    	
		    	javaBeanTransCommand.value = "";
		    }
		};
		
		ajax.open("GET", "transcontinental.jsp?concurrent=" + req, false);
		ajax.send();
		
	} else 
		document.getElementById("lblHint").innerHTML = "Server Failed";
	
}
//=========================================================================================================================================
function hint() {
	steps = [];

	if(!isAccomplished()) {
		getSteps();
		
		if(steps.length != 0) {
			var hintStep = "";
			for(var i = 0; i < steps.length; i ++) 
				hintStep += command_s[steps[i]] + "<br>";
			
			lblHint.innerHTML = hintStep;
			
		} else lblHint.innerHTML = "Not Solvable";
	} else lblHint.innerHTML = "Goal";

}

//=========================================================================================================================================
function canvasMove(cmd) {
	var str = "";
	var movCanvas;
	for(var i = 0; i < canvasArray.length; i ++) {
		for(var j = 0; j < canvasArray[0].length; j ++) {
			if(canvasArray[i][j] != "canvas9") {
				var orgPos 		= $(document.getElementById(canvasArray[i][j])).data("orgPos");
				var tmpPos 		= $(document.getElementById(canvasArray[i][j])).data("tmpPos");
				var direction 	= moveDirection[tmpPos];
				var dirString	= moveDirString[tmpPos];
				str += (canvasArray[i][j] + "->");
				for(var k = 0; k < direction.length; k ++)
						if(direction[k] != 0 &&  freeSpace == direction[k] && dirString[k] == cmd) {
							str += dirString[k];
							movCanvas = canvasArray[i][j];
						}	
				
				str += "\n"
					
			}
		}
	}
	
	str += "Canvas: " + movCanvas + "";
	debugViewWithInput(str);
	
	return movCanvas;
}

function moveUp() {
	var canvas = canvasMove("Up");
	if(canvas) {
		var tmpPosition = $(document.getElementById(canvas)).data("tmpPos");
		debugViewWithInput(canvas + "::moveUP()\n");
		var getCoordinate	= tmpCoorArray[freeSpace - 1];
		document.getElementById(canvas).style.left		= getCoordinate[0] + "px";
		document.getElementById(canvas).style.top		= getCoordinate[1] + "px";
		
		$(document.getElementById(canvas)).data("tmpPos", freeSpace);
		freeSpace = tmpPosition;
		document.getElementById("lblHint").innerHTML = command_s[0];
	}
}

//=========================================================================================================================================
function moveLeft() {
	var canvas = canvasMove("Left");
	if(canvas) {
		var tmpPosition = $(document.getElementById(canvas)).data("tmpPos");
		debugViewWithInput(canvas + "::moveLeft()\n");
		var getCoordinate	= tmpCoorArray[freeSpace - 1];
		document.getElementById(canvas).style.left		= getCoordinate[0] + "px";
		document.getElementById(canvas).style.top		= getCoordinate[1] + "px";
		
		$(document.getElementById(canvas)).data("tmpPos", freeSpace);
		freeSpace = tmpPosition;
		document.getElementById("lblHint").innerHTML = command_s[2];
	}
}

//=========================================================================================================================================
function moveDown() {
	var canvas = canvasMove("Down");
	if(canvas) {
		var tmpPosition = $(document.getElementById(canvas)).data("tmpPos");
		debugViewWithInput(canvas + "::moveDown()\n");
		var getCoordinate	= tmpCoorArray[freeSpace - 1];
		document.getElementById(canvas).style.left		= getCoordinate[0] + "px";
		document.getElementById(canvas).style.top		= getCoordinate[1] + "px";
		
		$(document.getElementById(canvas)).data("tmpPos", freeSpace);
		freeSpace = tmpPosition;
		document.getElementById("lblHint").innerHTML = command_s[1];
	}
}

//=========================================================================================================================================
function moveRight() {
	var canvas = canvasMove("Right");
	if(canvas) {
		var tmpPosition = $(document.getElementById(canvas)).data("tmpPos");
		debugViewWithInput(canvas + "::moveRight()\n");
		var getCoordinate	= tmpCoorArray[freeSpace - 1];
		document.getElementById(canvas).style.left		= getCoordinate[0] + "px";
		document.getElementById(canvas).style.top		= getCoordinate[1] + "px";
		
		$(document.getElementById(canvas)).data("tmpPos", freeSpace);
		freeSpace = tmpPosition;
		
		document.getElementById("lblHint").innerHTML = command_s[3];
	}
}

//=========================================================================================================================================
function runAutomatize() {
	
	if(!isAccomplished() && isProcessing == false) {
		
		if(isSolvable()) {
			getSteps();
			if(steps.length != 0) {
				
				isProcessing = true;
				
				var stepsCollection = [];
				for(var i = 0; i < steps.length; i ++) 
					if(steps[i] != -1)
						stepsCollection[i] = command[steps[i]];
				
				var timeout = 100;
				
				for(var i = 0; i < stepsCollection.length; i ++) {
					timeout += 200;
					if(stepsCollection[i] == "Up") 
						setTimeout(moveUp, timeout);
					else if(stepsCollection[i] == "Left") 
						setTimeout(moveLeft, timeout);
					else if(stepsCollection[i] == "Down") 
						setTimeout(moveDown, timeout);
					else if(stepsCollection[i] == "Right") 
						setTimeout(moveRight, timeout);
				}
				
				setTimeout(function() {
					if(isAccomplished()) {
						document.getElementById(canvasArray[canvasArray.length - 1][canvasArray[0].length - 1]).style.left = tmpCoorArray[8][0] + "px";
						freeSpace = -1;
						getConcurrentCanvasArray();
						document.getElementById("lblHint").innerHTML = "Goal<br>With Automate";
						isProcessing = false;
						//location.reload();
					}
				}, timeout += 300);
				
			} else 
				lblHint.innerHTML = "Not Solvable";

		} else 
			lblHint.innerHTML = "Not Solvable";
		
	} else document.getElementById("lblHint").innerHTML = "Goal";
 
}

//=========================================================================================================================================
function debugView() {
	var canvas 			= "ID: " + $(this).attr("id") + "\n";
	var accomplished 	= "IsGoal: " + isAccomplished() + "\n";
	var pos 			= "Pos(org, tmp): " + $(this).data("orgPos") + ", " + $(this).data("tmpPos") + "\n";
	var corX 			= new Number(tmpCoorArray[parseInt($(this).data("tmpPos") - 1)][0]).toFixed(0);
	var corY 			= new Number(tmpCoorArray[parseInt($(this).data("tmpPos") - 1)][1]).toFixed(0);
	var xyaxis 			= "XY-Axis: " + corX + ", " + corY + "\n";
	var startXY			= "StartXY: " + startX + ", " + startY + "\n"
	var solvable 		= "Is Solvable: " + isSolvable() + "\n";
 	
	var strCCA = "";
	for(var i = 0; i < concurrentCanvasArray.length; i ++) {
		var raw = "";
		for(var j = 0; j < concurrentCanvasArray[0].length; j ++) {
			raw += concurrentCanvasArray[i][j] + ", ";
		}
		strCCA += "Row " + i + ": [" + raw.substring(0, raw.length - 2) + "]\n";
	}
	
	var txt = "***** Debug *****\n" + canvas + accomplished + solvable + pos + xyaxis + startXY + strCCA;
	txt += "\n";
	
	$("#debugView").append(txt);
	document.getElementById("debugView").scrollTop = document.getElementById("debugView").scrollHeight; 
	
}

function debugViewWithInput(txt) {
	$("#debugView").append(txt + "\n");
	document.getElementById("debugView").scrollTop = document.getElementById("debugView").scrollHeight; 
}
//=========================================================================================================================================
function eventRegister() {
	for(var i = 0; i < canvasArray.length; i ++) {
		for(var j = 0; j < canvasArray[0].length; j ++) { 
			document.getElementById(canvasArray[i][j]).onmouseover	= debugView;
			document.getElementById(canvasArray[i][j]).onclick		= brickTrans;
		}
	}
	
	for(var i = 0; i < imgArray.length; i ++)
		document.getElementById(imgArray[i]).onclick = splitImg;
	
	btnSplit.onclick 	= randomDismiss;
	btnHint.onclick 	= hint; 
	btnAuto.onclick 	= runAutomatize;
	
}
window.onload = eventRegister;
//=========================================================================================================================================

