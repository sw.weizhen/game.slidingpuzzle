package javabean.algorithm;

public class Exam {
	
	private String arg;
	
	public Exam() {
		System.out.println("Exam::Exam()");
		
		this.arg = "Default value";
	}
	
	public void setExampara(String arg) {
		this.arg += arg;
	}
	
	public String getExampara() {
		return this.arg;
	}

}
