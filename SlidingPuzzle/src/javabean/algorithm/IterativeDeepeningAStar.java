package javabean.algorithm;

public class IterativeDeepeningAStar {
	
	private final int goalPosition[][] = 	{
												{2,2},	
												{0,0},
												{0,1},
												{0,2},
												{1,0},
												{1,1},
												{1,2},
												{2,0},
												{2,1}
											};
	
	//private final String command[] = 	{"Up", "Down", "Left", "Right"};
	private final int directX[] = 		{-1,  1,  0,  0};
	private final int directY[] = 		{ 0,  0, -1,  1};
	private final int relDirect[] = 	{ 1,  0,  3,  2};

	private int concurrentPuzzle[][];
	private int steps[];
	
	private int startX;
    private int startY;
    
    private int bound;
    private boolean goal;
    

	public IterativeDeepeningAStar() {
		
	}
	
	
	public String getOptimalSteps() {
		if(this.isSolvable(this.concurrentPuzzle)) {
			while(!this.goal) 
				this.bound = this.IDAstar(this.startX, this.startY, 0, -1);
			
			String command = "";
			for(int i = 0; i < this.steps.length; i ++)
				if(this.steps[i] != -1)
					command += this.steps[i] + ",";
			
			command = command.substring(0, command.length() - 1).trim();
			
			return command;
			
		} else return "";
	}
	
	
	public void setConcurrentPuzzle(String puzzle) {
		this.concurrentPuzzle = new int[3][3];
		this.steps = new int[50];
		
		this.startX = 0;
		this.startY = 0;
		
		this.bound = 0;
		this.goal = false;
		
		String puzzleArray[] = puzzle.split(",");
		for(int i = 0; i < this.concurrentPuzzle.length; i ++) 
			for(int j = 0; j < this.concurrentPuzzle[0].length; j ++) {
				this.concurrentPuzzle[i][j] = Integer.parseInt(puzzleArray[(i * this.concurrentPuzzle.length) + j].trim());
				if(this.concurrentPuzzle[i][j] == 0) {
					this.startX = i;
					this.startY = j;
				}
			}
		
		for(int i = 0; i < this.steps.length; i ++)
			this.steps[i] = -1;
	}
	
	public void showConcurrentPuzzle() {
		for(int i = 0; i < concurrentPuzzle.length; i ++) {
			for(int j = 0; j < concurrentPuzzle[0].length; j ++) {
				System.out.print(concurrentPuzzle[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println("StartX: " + this.startX + "  StartY: " + this.startY);
	}
	
	
	private int manhattanDistance(int x1, int y1, int x2, int y2) {
        return Math.abs(x1 - x2) + Math.abs(y1 - y2);
    }
	
	
	private boolean puzzleLimit(int x, int y) {
	    return x >= 0 && x < 3 && y >= 0 && y < 3;
	}
	
	
	private void swap(int nx, int ny, int x, int y) {
		int tmpValue = 				concurrentPuzzle[nx][ny];
		concurrentPuzzle[nx][ny] = 	concurrentPuzzle[x][y];
		concurrentPuzzle[x][y] = 	tmpValue;
	}
	
	
	private int heuristic(int slidingPuzzle[][]) {
	    int cost = 0;
	    for (int i = 0; i < 3; i ++)
	        for (int j = 0; j < 3; j ++)
	            if (slidingPuzzle[i][j] != 0) 
	                cost += manhattanDistance(i, j, goalPosition[slidingPuzzle[i][j]][0], goalPosition[slidingPuzzle[i][j]][1]);
	    
	    return cost;
	}
	
	
	private boolean isSolvable(int[][] puzzle) {
	    int inversions = 0;
	    int[] puzzleArray = new int[9];
	    int k = 0;
	    
	    for(int i = 0; i < puzzle.length; i ++)
	    	for(int j =0; j < puzzle[0].length; j ++)
	    		puzzleArray[k++] = puzzle[i][j];

	    for(int i = 0; i < puzzleArray.length - 1; i ++) {
	      for(int j = i + 1; j < puzzleArray.length; j ++) 
	    	  if(puzzleArray[i] > puzzleArray[j]) 
	    		  inversions ++;  
	      
	      if(puzzleArray[i] == 0 && i % 2 == 1)
	    	  inversions ++;
	    }

	    return (inversions % 2 == 0);
	 }
	
	
	private int IDAstar(int x, int y, int Gn, int prev_dir) {
		int Hn = heuristic(this.concurrentPuzzle);
	    if (Hn == 0) {
	    	goal = true;
	    	return Gn;
	    }
	    
	    if (Gn +  Hn > bound) 
	    	return Gn + Hn;

	    int nextBound = 999999;
	    for (int i = 0; i < 4; ++ i) {
	        int nx = x - this.directX[i];
	        int ny = y - this.directY[i];
	 
	        if (this.relDirect[i] == prev_dir || !this.puzzleLimit(nx, ny)) continue;
	        
	        this.steps[Gn] = i;
	        this.swap(nx, ny, x, y);
	        
	        int c = IDAstar(nx, ny, Gn + 1, i);
	        if (goal)	
	        	return c;
	        
	        nextBound = Math.min(nextBound, c);
	        swap(x, y, nx, ny); 
	    }
	    return nextBound;
	}
	
	
	
}
