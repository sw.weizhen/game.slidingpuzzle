var steps 		= 	[-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1];
var imgArray 	= 	["img00", "img01", "img02", "img03", "img04"];

var canvasArray = 	[
						["canvas1", "canvas2", "canvas3"],
						["canvas4", "canvas5", "canvas6"],
						["canvas7", "canvas8", "canvas9"]
					];

var concurrentCanvasArray = [
								[1, 2, 3],
								[4, 5, 6],
								[7, 8, 0]
							];

var tmpCoorArray = [
						[0, 0],
						[0, 0],
						[0, 0],
						[0, 0],
						[0, 0],
						[0, 0],
						[0, 0],
						[0, 0],
						[0, 0],
					];

var moveDirection = [
						[0, 0, 0, 0],
						[2, 4, 0, 0], //1
						[1, 3, 5, 0], //2
						[2, 6, 0, 0], //3
						[1, 5, 7, 0], //4
						[2, 4, 6, 8], //5
						[3, 5, 9, 0], //6
						[4, 8, 0, 0], //7
						[5, 7, 9, 0], //8
						[6, 8, 0, 0]  //9
					];


var moveDirString = [
						["None", 	"None", 	"None", 	"None"],
						["Right", 	"Down", 	"None", 	"None"], //1
						["Left", 	"Right", 	"Down", 	"None"], //2
						["Left", 	"Down", 	"None", 	"None"], //3
						["Up", 		"Right", 	"Down", 	"None"], //4
						["Up", 		"Left", 	"Right", 	"Down"], //5
						["Up", 		"Left", 	"Down", 	"None"], //6
						["Up", 		"Right", 	"None", 	"None"], //7
						["Up", 		"Left", 	"Right", 	"None"], //8
						["Up", 		"Left", 	"None", 	"None"], //9
					];

//position -> goal::0::coordinate(2, 2), counting position from 1
var goalPosition = 		[
							[2,2],	//initial misplace with index = 0 coordinate = 2, 2
							[0,0],  //1
							[0,1],  //2
							[0,2],  //3
							[1,0],  //4
							[1,1],  //5
							[1,2],  //6
							[2,0],  //7
							[2,1]   //8
						];

var goalPositionArray = [
							[1, 2, 3],
							[4, 5, 6],
							[7, 8, 0]
						];


var coordinateLat = 	[
							[1, 2, 3],
							[4, 5, 6],
							[7, 8, 0]
						];

var command 	= 	["Up", "Down", "Left", "Right"]; //output string
var command_s	=	["&uarr;", "&darr;", "&larr;", "&rarr;"];
var directX 	= 	[-1,  1,  0,  0]; //logic direct up down
var directY 	= 	[ 0,  0, -1,  1]; //logic direct left right
var relDirect 	= 	[ 1,  0,  3,  2]; //logic direct command


var freeSpace = 9;

var orgWidth	= 0;
var orgHeight	= 0;

var picGap = 5;

var secW = 0;
var secH = 0;

var startX 		= 2;
var startY 		= 2;

var goal 		= false;
var bound 		= 0;
var loopCount 	= 0;

var count = 0;

var isProcessing = false;

//=========================================================================================================================================
function splitImg() {
	var imgObj = document.getElementById($(this).data("mapping"));
	
	orgWidth	= imgObj.width;
    orgHeight	= imgObj.height;
    
    secW = orgWidth / 3;
    secH = orgHeight / 3;

    canvasMain.width	= orgWidth;
    canvasMain.height	= orgHeight;
    
    canvasMain.style.width	= orgWidth;
    canvasMain.style.height	= orgHeight;

	var x = 0;
	var y = 0;
	var split;
	
	var pX = 0; 
	var pY = 0;
	var k = 0;
	var dataPos = 1;
	
	for(var i = 0; i < canvasArray.length; i ++) {
		for(var j = 0; j < canvasArray[0].length; j ++) {
			
			document.getElementById(canvasArray[i][j]).width	= secW;
	    	document.getElementById(canvasArray[i][j]).height	= secH;
			
			split = canvasMain.getContext("2d");
			split.rect(x, y, secW, secH);
			split.drawImage(imgObj, 0, 0);
			document.getElementById(canvasArray[i][j]).getContext("2d").drawImage(canvasMain, x, y, secW, secH, 0, 0, secW, secH);

		    document.getElementById(canvasArray[i][j]).style.top	= pY + "px";
			document.getElementById(canvasArray[i][j]).style.left	= pX + "px";
			
			$(document.getElementById(canvasArray[i][j])).data("orgPos", dataPos);
			$(document.getElementById(canvasArray[i][j])).data("tmpPos", dataPos ++);
			
			tmpCoorArray[k][0] = pX;
			tmpCoorArray[k][1] = pY;

			pX += secW + picGap;
			
			x += secW;
			k ++;
		}
		
		x = 0;
		y += secH;
		
		pX = 0;
		pY += secH + picGap;
	}

	freeSpace = -1;
	
	
}

//=========================================================================================================================================
function randomDismiss() {
	var randomList = [];
	var randomPos;
	
	for(;;) {
		randomPos = parseInt(Math.floor(Math.random() * 8) + 1);
		if(randomList.indexOf(randomPos) == -1) randomList.push(randomPos);
		if(randomList.length >= 8) break;
	}
	
	var tmpPosition = 0;
	var k = 0;
	for(var i = 0; i < canvasArray.length; i ++) {
		for(var j = 0; j < canvasArray[0].length; j ++) { 
			$(document.getElementById(canvasArray[i][j])).data("tmpPos", randomList[k ++]);
			
			tmpPosition = parseInt($(document.getElementById(canvasArray[i][j])).data("tmpPos"));
			document.getElementById(canvasArray[i][j]).style.top = tmpCoorArray[tmpPosition - 1][1] + "px";
			document.getElementById(canvasArray[i][j]).style.left = tmpCoorArray[tmpPosition - 1][0] + "px";
			
		}
	}
	document.getElementById(canvasArray[canvasArray.length - 1][canvasArray[0].length - 1]).style.left = (secW * (canvasArray.length) + picGap * 3) + "px";
	
	freeSpace = 9;
	
	getConcurrentCanvasArray();
	
	document.getElementById("lblHint").innerHTML = "PLAYING";
}

//=========================================================================================================================================
function getConcurrentCanvasArray() {
	var tmpPos = 0;
	var orgPos = 0;
	
	var ei 	= 0;
	var ej 	= 0
	
	for(var i = 0; i < canvasArray.length; i ++) {
		for(var j = 0; j < canvasArray[0].length; j ++) {
			tmpPos 	= $(document.getElementById(canvasArray[i][j])).data("tmpPos");
			orgPos 	= $(document.getElementById(canvasArray[i][j])).data("orgPos");
			ei 		= parseInt((tmpPos - 1) / 3);
			ej 		= (tmpPos - 1) % 3;
			
			orgPos == 9 ? orgPos = 0: orgPos = orgPos;
			concurrentCanvasArray[ei][ej] = orgPos;
		}
	}
	
	var transConcurrent = "";
	for(var i = 0; i < concurrentCanvasArray.length; i ++) 
		for(var j = 0; j < concurrentCanvasArray[0].length; j ++)
			transConcurrent += concurrentCanvasArray[i][j] + ",";
	javaBeanTransConcurrent.value = transConcurrent.substring(0, transConcurrent.length - 1);
}

//=========================================================================================================================================
function isAccomplished() {
	var result = true;
	var orgPos = 0;
	var tmpPos = 0;
	for(var i = 0; i < canvasArray.length; i ++) {
		for(var j = 0; j < canvasArray[0].length; j ++) { 
			orgPos = $(document.getElementById(canvasArray[i][j])).data("orgPos");
			tmpPos = $(document.getElementById(canvasArray[i][j])).data("tmpPos");
			if(parseInt(orgPos) != parseInt(tmpPos)) return false;
		}
	}
	
	return result;
}

//=========================================================================================================================================
function isSolvable() {
	var arrayPuzzle = [];
	var k = 0; 
	for(var i = 0; i < concurrentCanvasArray.length; i ++)
		for(var j = 0; j < concurrentCanvasArray[0].length; j ++)
			arrayPuzzle.push(concurrentCanvasArray[i][j]);
	
	var inversions = 0;
    for(var i = 0; i < arrayPuzzle.length - 1; i ++) {
      for(var j = i + 1; j < arrayPuzzle.length; j ++) 
    	  if(arrayPuzzle[i] > arrayPuzzle[j]) 
    		  inversions ++;  
      
      if(arrayPuzzle[i] == 0 && i % 2 == 1)
    	  inversions ++;
    }

    return (inversions % 2 == 0);
}

//=========================================================================================================================================
function swap(i, j, ei, ej) {
	var tmpValue 					= concurrentCanvasArray[i][j];
	concurrentCanvasArray[i][j] 	= concurrentCanvasArray[ei][ej];
	concurrentCanvasArray[ei][ej] 	= tmpValue;
}

//=========================================================================================================================================
function brickTrans() {
	if(!isAccomplished()) {
		var tmpPosition = $(this).data("tmpPos");
		var orgPosition = $(this).data("orgPos");
		var dir = 0;
		var direction = moveDirection[tmpPosition];
		
		var concurrent_i = parseInt((tmpPosition - 1) / 3);
		var concurrent_j = (tmpPosition - 1) % 3;
		
		for(var i = 0; i < direction.length; i ++) {
			dir = direction[i];
			if(dir != 0 && (parseInt(dir) == parseInt(freeSpace)) && orgPosition != 9) {
				var getCoordinate	= tmpCoorArray[freeSpace - 1];
				this.style.left		= getCoordinate[0] + "px";
				this.style.top		= getCoordinate[1] + "px";
				
				$(this).data("tmpPos", freeSpace);
				freeSpace = tmpPosition;
			}
		}
		
		swap(concurrent_i, concurrent_j, parseInt(($(this).data("tmpPos") - 1) / 3), ($(this).data("tmpPos") - 1) % 3);
		startX = concurrent_i;
		startY = concurrent_j;
		
		var transConcurrent = "";
		for(var i = 0; i < concurrentCanvasArray.length; i ++) 
			for(var j = 0; j < concurrentCanvasArray[0].length; j ++)
				transConcurrent += concurrentCanvasArray[i][j] + ",";
		javaBeanTransConcurrent.value = transConcurrent.substring(0, transConcurrent.length - 1);

		if(isAccomplished()) {
			document.getElementById(canvasArray[canvasArray.length - 1][canvasArray[0].length - 1]).style.left = tmpCoorArray[8][0] + "px";
			freeSpace = -1;
			getConcurrentCanvasArray();
			document.getElementById("lblHint").innerHTML = "GOAL";
		}
	}
}

//=========================================================================================================================================
function hint() {
	var hint = "1: UP<br>2: DOWN<br>3: LEFT<br>4: RIGHT<br>";
	document.getElementById("lblHint").innerHTML = hint;
}

//=========================================================================================================================================
function heuristic() {
    var cost = 0;
    for (var i = 0; i < 3; i ++)
        for (var j = 0; j < 3; j ++)
            if (concurrentCanvasArray[i][j] != 0) 
                cost += manhattanDistance(i, j, goalPosition[concurrentCanvasArray[i][j]][0], goalPosition[concurrentCanvasArray[i][j]][1]);
    return cost;
}

//=========================================================================================================================================
function manhattanDistance(x1, y1, x2, y2) {
    return Math.abs(x1 - x2) + Math.abs(y1 - y2);
}

//=========================================================================================================================================
 function puzzleLimit(x, y) {
    return x >= 0 && x < 3 && y >= 0 && y < 3;
}

//=========================================================================================================================================
function IDAstar(x, y, Gn, prev_dir) {
 	loopCount ++;
     var Hn = heuristic(concurrentCanvasArray);
     
     if (Hn == 0) {
     	goal = true;
     	return Gn;
     }
     
     if (Gn +  Hn > bound) 
     	return Gn + Hn;

     var nextBound = 99999;
     for (var i = 0; i < 4; ++ i) {
         var nx = x - directX[i];
         var ny = y - directY[i];
  
         if (relDirect[i] == prev_dir || !puzzleLimit(nx, ny)) continue;
         
         steps[Gn] = i;
         swap(nx, ny, x, y);
         
         var c = IDAstar(nx, ny, Gn + 1, i);
         if (goal)	
         	return c;
         
         nextBound = Math.min(nextBound, c);
  
         swap(x, y, nx, ny); 
     }
     
     return nextBound;
 }
//=========================================================================================================================================
function puzzleExecute() {
	var str = "";
	var solvable = isSolvable();
	str += "Solvable: " + solvable + "\n";
	
	for(var i = 0; i < steps.length; i ++)
		steps[i] = -1;

	for(var i = 0; i < concurrentCanvasArray.length; i ++)
		for(var j = 0; j < concurrentCanvasArray[0].length; j ++)
			coordinateLat[i][j] = concurrentCanvasArray[i][j];
	
	goal 		= false;
	bound 		= 0;
	loopCount 	= 0;
	
	if(solvable) {
		while(!goal)
			bound = IDAstar(startX, startY, 0, -1);
		
		for(var i = 0; i < steps.length; i ++)
			if(steps[i] != -1)
				str += (i + 1) + "	Step: " + command[steps[i]] + "\n";
	}
	
	for(var i = 0; i < concurrentCanvasArray.length; i ++)
		for(var j = 0; j < concurrentCanvasArray[0].length; j ++)
			concurrentCanvasArray[i][j] = coordinateLat[i][j];
	
	
	debugViewWithInput("*** GET STEPS ***\n" + str);
}

//=========================================================================================================================================
function hintSteps() {
	if(isSolvable()) {
		var hint 			= [];
		var hintDebugView 	= "";
		var hintShow 		= "";
		
		puzzleExecute();
		
		for(var i = 0; i < steps.length; i ++)
			if(steps[i] != -1) {
				hint[i] = command_s[steps[i]] + "  ";
				if(i % 5 == 0) 
					hintDebugView += "\n";
				
				hintDebugView 	+= hint[i] + " ";
				hintShow 	+= hint[i] + "<br>";
			}
		
		document.getElementById("lblHint").innerHTML = hintShow;
		
	} else {
		document.getElementById("lblHint").innerHTML = "OOPS!<br>NOT SOLVABLE!<br>SPLIT AGAIN!";
	}
}

//=========================================================================================================================================
//for keypad
function canvasMove(cmd) {
	var str = "";
	var movCanvas;
	for(var i = 0; i < canvasArray.length; i ++) {
		for(var j = 0; j < canvasArray[0].length; j ++) {
			if(canvasArray[i][j] != "canvas9") {
				var orgPos 		= $(document.getElementById(canvasArray[i][j])).data("orgPos");
				var tmpPos 		= $(document.getElementById(canvasArray[i][j])).data("tmpPos");
				var direction 	= moveDirection[tmpPos];
				var dirString	= moveDirString[tmpPos];
				str += (canvasArray[i][j] + "->");
				for(var k = 0; k < direction.length; k ++) {
						if(direction[k] != 0 &&  freeSpace == direction[k] && dirString[k] == cmd) {
							str += dirString[k];
							movCanvas = canvasArray[i][j];
						}	
				}
				str += "\n"
					
			}
		}
	}
	
	str += "Canvas: " + movCanvas + "";
	debugViewWithInput(str);
	
	return movCanvas;
}

//=========================================================================================================================================
function moveUp() {
	var canvas = canvasMove("Up");
	if(canvas) {
		var tmpPosition = $(document.getElementById(canvas)).data("tmpPos");
		debugViewWithInput(canvas + "::moveUP()\n");
		var getCoordinate	= tmpCoorArray[freeSpace - 1];
		document.getElementById(canvas).style.left		= getCoordinate[0] + "px";
		document.getElementById(canvas).style.top		= getCoordinate[1] + "px";
		
		$(document.getElementById(canvas)).data("tmpPos", freeSpace);
		freeSpace = tmpPosition;
		document.getElementById("lblHint").innerHTML = command_s[0];
	}
}

//=========================================================================================================================================
function moveLeft() {
	var canvas = canvasMove("Left");
	if(canvas) {
		var tmpPosition = $(document.getElementById(canvas)).data("tmpPos");
		debugViewWithInput(canvas + "::moveLeft()\n");
		var getCoordinate	= tmpCoorArray[freeSpace - 1];
		document.getElementById(canvas).style.left		= getCoordinate[0] + "px";
		document.getElementById(canvas).style.top		= getCoordinate[1] + "px";
		
		$(document.getElementById(canvas)).data("tmpPos", freeSpace);
		freeSpace = tmpPosition;
		document.getElementById("lblHint").innerHTML = command_s[2];
	}
}

//=========================================================================================================================================
function moveDown() {
	var canvas = canvasMove("Down");
	if(canvas) {
		var tmpPosition = $(document.getElementById(canvas)).data("tmpPos");
		debugViewWithInput(canvas + "::moveDown()\n");
		var getCoordinate	= tmpCoorArray[freeSpace - 1];
		document.getElementById(canvas).style.left		= getCoordinate[0] + "px";
		document.getElementById(canvas).style.top		= getCoordinate[1] + "px";
		
		$(document.getElementById(canvas)).data("tmpPos", freeSpace);
		freeSpace = tmpPosition;
		document.getElementById("lblHint").innerHTML = command_s[1];
	}
}

//=========================================================================================================================================
function moveRight() {
	var canvas = canvasMove("Right");
	if(canvas) {
		var tmpPosition = $(document.getElementById(canvas)).data("tmpPos");
		debugViewWithInput(canvas + "::moveRight()\n");
		var getCoordinate	= tmpCoorArray[freeSpace - 1];
		document.getElementById(canvas).style.left		= getCoordinate[0] + "px";
		document.getElementById(canvas).style.top		= getCoordinate[1] + "px";
		
		$(document.getElementById(canvas)).data("tmpPos", freeSpace);
		freeSpace = tmpPosition;
		
		document.getElementById("lblHint").innerHTML = command_s[3];
	}
}

//=========================================================================================================================================
function runAutomatize() {
	var stepsCollection = [];
	for(var i = 0; i < steps.length; i ++) 
		if(steps[i] != -1)
			stepsCollection[i] = command[steps[i]];
	var timeout = 100;

	for(var i = 0; i < stepsCollection.length; i ++) {	
		timeout += 200;
		if(stepsCollection[i] == "Up") 
			setTimeout(moveUp, timeout);
		else if(stepsCollection[i] == "Left") 
			setTimeout(moveLeft, timeout);
		else if(stepsCollection[i] == "Down") 
			setTimeout(moveDown, timeout);
		else if(stepsCollection[i] == "Right") 
			setTimeout(moveRight, timeout);
	}
	
	setTimeout(function(){
		if(isAccomplished()) {
			document.getElementById(canvasArray[canvasArray.length - 1][canvasArray[0].length - 1]).style.left = tmpCoorArray[8][0] + "px";
			freeSpace = -1;
			getConcurrentCanvasArray();
			document.getElementById("lblHint").innerHTML = "GOAL<br>WITH AUTOMATE";
			
			isProcessing = false;
		}
	}, timeout += 300);
}

//=========================================================================================================================================
function debugView() {
	
	var canvas 			= "ID: " + $(this).attr("id") + "\n";
	var accomplished 	= "IsGoal: " + isAccomplished() + "\n";
	var pos 			= "Pos(org, tmp): " + $(this).data("orgPos") + ", " + $(this).data("tmpPos") + "\n";
	var corX 			= new Number(tmpCoorArray[parseInt($(this).data("tmpPos") - 1)][0]).toFixed(0);
	var corY 			= new Number(tmpCoorArray[parseInt($(this).data("tmpPos") - 1)][1]).toFixed(0);
	var xyaxis 			= "XY-Axis: " + corX + ", " + corY + "\n";
	var startXY			= "StartXY: " + startX + ", " + startY + "\n"
	var solvable 		= "Is Solvable: " + isSolvable() + "\n";
 	
	var strCCA = "";
	for(var i = 0; i < concurrentCanvasArray.length; i ++) {
		var raw = "";
		for(var j = 0; j < concurrentCanvasArray[0].length; j ++) {
			raw += concurrentCanvasArray[i][j] + ", ";
		}
		strCCA += "Row " + i + ": [" + raw.substring(0, raw.length - 2) + "]\n";
	}
	
	var txt = "***** Debug *****\n" + canvas + accomplished + solvable + pos + xyaxis + startXY + strCCA;
	txt += "\n";
	
	$("#debugView").append(txt);
	document.getElementById("debugView").scrollTop = document.getElementById("debugView").scrollHeight; 
	
}

function debugViewWithInput(txt) {
	$("#debugView").append(txt + "\n");
	document.getElementById("debugView").scrollTop = document.getElementById("debugView").scrollHeight; 
}
//=========================================================================================================================================
function implement() {
	if(isSolvable() && isProcessing == false) {
		isProcessing = true;
		document.getElementById("lblHint").innerHTML = "AUTOMATE SLIDING";
		puzzleExecute();
		runAutomatize();
	} else {
		document.getElementById("lblHint").innerHTML = "OOPS<br>NOT SOLVABLE<br>SPLIT AGAIN!";
	}
}
//=========================================================================================================================================

function buttonTest() {
	alert("XX");
	//debugViewWithInput("OO       XX");
}

function eventRegister() {
	for(var i = 0; i < canvasArray.length; i ++) {
		for(var j = 0; j < canvasArray[0].length; j ++) { 
			document.getElementById(canvasArray[i][j]).onmouseover	= debugView;
			document.getElementById(canvasArray[i][j]).onclick		= brickTrans;
		}
	}
	
	for(var i = 0; i < imgArray.length; i ++)
		document.getElementById(imgArray[i]).onclick = splitImg;
	
	btnSplit.onclick 	= randomDismiss;
	btnHint.onclick		= hintSteps;
	btnAuto.onclick		= implement;
	
}
window.onload = eventRegister;

